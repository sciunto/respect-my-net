$(document).ready(function() {
   var onCountryClick = function(target) {
      window.location='/list/'+target.id+"/";
   };
   $(function() {
	/* we're gonna calculate the colors for our countries */
	var dataset = {}
	var onlyValues = data.map(function(obj){ return obj.w});
	var minValue = Math.min.apply(null, onlyValues),
		maxValue = Math.max.apply(null, onlyValues);
	var paletteScale = d3.scale.linear()
		.domain([minValue, maxValue])
		.range(['#EFEFEF', '#02386F']);
	data.forEach(function(item) {
		var iso = item.iso2,
			value = item.w;
		dataset[iso] = { numberOfCases: value, fillColor: paletteScale(value)};
	});

	if (country) {
		dataset[country] = {fillColor: '#02386F'};
	}
	
	$('#map').datamaps({
		element: document.getElementById('map'),
		scope: 'europe',
		setProjection: function(element, options) {
			var projection, path;
			projection = d3.geo.stereographic()
				.center([15.22574, 54.2361])
				.scale(element.offsetWidth * 2)
				.translate([element.offsetWidth / 2, element.offsetHeight / 2]);
			path = d3.geo.path()
				.projection(projection);
			return {path: path, projection: projection}
		},
		data: dataset,
		fills: {defaultFill: '#F5F5F5'},
		geographyConfig: {
			dataUrl: '/static/europe.topojson',
			borderColor: '#DEDEDE',
			highlightBorderWidth: 2,
			highLightFillColor: function(geo) {
				return geo['fillColor'] || '#F5F5F5';
			},
			highlightBorderColor: '#B7B7B7',
			popupTemplate: function(geo, data) {
				if (!data){ return };
				return ['<div class="hoverinfo">',
					'<strong>', geo.properties.NAME, '</strong>',
					'<br><strong>', data.numberOfCases, '</strong> Reported cases',
					'</div>'].join('')
			}
		},
		done: function(datamap) {
			datamap.svg.selectAll('.datamaps-subunit').on('click', function(geography) {
				window.location='/list/'+geography.id+"/";
			});
		}
	});
    });
});
